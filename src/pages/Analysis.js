import React, { useEffect, } from 'react';

import '../styles/Analysis.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChartArea } from '@fortawesome/free-solid-svg-icons';

import { 
  useSelector,
  useDispatch,
} from 'react-redux';

import {
  //portfolioDataSelector,
  getCoinHistory,
  setSettings,
  coinHistorySelector,
  coinDataSelector,
  settingsSelector,
} from '../redux/apiCallsSlice';

import Chart from '../components/chart';
import CustomSelect from '../components/customselect';

import {getDate} from '../functions/functions';
import SettingsManager from '../functions/settingsmanager';

/*

  --blue: #375a7f;
  --indigo: #6610f2;
  --purple: #6f42c1;
  --pink: #e83e8c;
  --red: #E74C3C;
  --orange: #fd7e14;
  --yellow: #F39C12;
  --green: #00bc8c;
  --teal: #20c997;
  --cyan: #3498DB;
  --white: #fff;
  --gray: #999;
  --gray-dark: #303030;
  --primary: #375a7f;
  --secondary: #444;
  --success: #00bc8c;
  --info: #3498DB;
  --warning: #F39C12;
  --danger: #E74C3C;
  --light: #999;
  --dark: #303030;

*/

const Analysis = () => {

  const dispatch = useDispatch();
  const selectCoinData = useSelector(coinDataSelector);
  const selectCoinHistory = useSelector(coinHistorySelector);
  const selectSettings = useSelector(settingsSelector);

  const defaultCoinRequest = {
    coinId: "btc-bitcoin",
    quote: "usd",
    start: getDate('month'),
    end: getDate('today'),
  }

  const getCoinForChart = (coinRank) => {
    const coinCode = selectCoinData[coinRank].id;

    chartRefresh('coin-id', coinCode);
    const updatedSettings = SettingsManager.FormatSettingsObject(selectSettings, 'analysis-chart', 'coin-id', coinCode);

    dispatch(setSettings(updatedSettings));
    
    SettingsManager.Save(updatedSettings);
  }

  const chartRefresh = (prop, val) => {

    let change = {};
    switch (prop)  {
      case ('coin-id') : {
        change = {
          coinId: val
        };
        break;
      }
      case ('date-range') : {
        change = {
          start: getDate(val)
        };
        break;
      }
      case ('trading-pair') : {
        change = {
          quote: val
        };
        break;
      }
      //default is don't change anything
      default : {
        change = {};
      }
    }

    const newRequest = Object.assign(defaultCoinRequest, change);
    dispatch(getCoinHistory(newRequest));
  }

  const dropdownData = [
    {
      settingName : 'trading-pair',
      defaultItem: 'usd',
      itemList: [
        {label: 'USD',
        value: 'usd'},
        {label: 'BTC',
        value: 'btc'},
      ]
    },
    {
      settingName : 'date-range',
      defaultItem: 'month',
      itemList: [
        {label: 'Day',
        value: 'day'},
        {label: 'Week',
        value: 'week'},
        {label: 'Month',
        value: 'month'},
        {label: 'YTD',
        value: 'ytd'},
        {label: 'Year',
        value: 'year'},
    //    {label: 'Max',
    //    value: 'max'},
      ]
    }
  ];

  const chartOptions = {
    'dropdownData': dropdownData,
  }
  
  const buildChart = () => {
    if (selectCoinHistory.length > 0) {
      return (
        <Chart 
          chartId={'analysis-chart'}
          lineData={selectCoinHistory}
          options={chartOptions}
          chartRefresh={chartRefresh}
        />
      );
    }
  }

  useEffect(() => {

    let coinRequest = defaultCoinRequest;

    if (selectSettings['analysis-chart']) {
      if (selectSettings['analysis-chart']['coin-id'] !== undefined) {
        coinRequest['coinId'] = selectSettings['analysis-chart']['coin-id'];
      }
      if (selectSettings['analysis-chart']['trading-pair'] !== undefined) {
        coinRequest['quote'] = selectSettings['analysis-chart']['trading-pair'];
      }
      if (selectSettings['analysis-chart']['date-range'] !== undefined) {
        coinRequest['start'] = getDate(selectSettings['analysis-chart']['date-range']);
      }
    }

    dispatch(getCoinHistory(coinRequest));

    
    const setTimeoutSeconds = 1;
    const setIntervalSeconds = 10;
  
    /*
    setTimeout(() => {
      
      setInterval(() => {

        //const dt = new Date()

        //dispatch(getCoinData());
        dispatch(getCoinHistory(coinRequest));

        //dispatch(getCoinDataDemo());
        //dispatch(getCoinHistoryDemo());
      }, setIntervalSeconds*1000)
    }, setTimeoutSeconds*1000);
    */


  }, [dispatch]);

  return (
    <section id="coin-analysis">
      <div className="page-header"><span className="header-icon"><FontAwesomeIcon icon={faChartArea}/></span><h2>Analysis</h2></div>
      <div className="container">
      <div id="coin-select">
          <CustomSelect
            options={selectCoinData}
            dropdownCountLimit="5"
            placeholder="Type to Search..."
            selectedAction={getCoinForChart}
          />
        </div>
        <div className="chart">
          { buildChart() }
        </div>
      </div>
    </section>
  );
  
}

export default Analysis;