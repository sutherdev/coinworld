export function delayedAction(doAction) {
  setTimeout(() => { doAction(); }, 1);
}

export function findObjectByKey(array, key, value) {
  for (let i = 0; i < array.length; i++) {
    if (array[i][key] === value) return array[i];
  }
}

export function tidyRound(value, separator) {
  if (!value || value === 0) return value;
  
  let int = value.toString().split(".")[0];
  let dec = value.toString().split(".")[1];
  let numb = 0;

  if (int.length > 2 || !dec || dec === 0) {
    numb = Number(value).toFixed(0).toString();
    
    if (separator) numb = numb.replace(/\B(?=(\d{3})+(?!\d))/g, separator);;
  } else if (int.length === 2 || value > 0.05) {
    numb = Number(value).toFixed(2);
  } else {
    numb = Number(value).toFixed(5);
  }
  return numb;
}

export function toggleDisable(ele) {
  if (document.getElementById(ele).disabled) {
    document.getElementById(ele).disabled = false;
  } else {
    document.getElementById(ele).disabled = true;
  }
}

export function getWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}

export function getHeight() {
  return Math.max(
    document.body.scrollHeight,
    document.documentElement.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.offsetHeight,
    document.documentElement.clientHeight
  );
}


export function getDate(time) {
  const todayObj = new Date();
  const todayMonth = (todayObj.getMonth()+1 < 10) ? `0${todayObj.getMonth()+1}` : todayObj.getMonth()+1;
  const todayDate = (todayObj.getDate() < 10) ? `0${todayObj.getDate()}` : todayObj.getDate();
  const todayYear = todayObj.getFullYear();

  const monthAgo = (todayObj.getMonth() < 10) ? `0${todayObj.getMonth()}` : todayObj.getMonth();
  const weekAgo = ((todayObj.getDate() - 7) < 10) ? `0${(todayObj.getDate() - 7)}` : (todayObj.getDate() -7);
  const dayAgo = ((todayObj.getDate() - 1) < 10) ? `0${(todayObj.getDate() - 1)}` : (todayObj.getDate() - 1);

  switch (time) {
    case 'today'  : return `${todayYear}-${todayMonth}-${todayDate}`;
    case 'day'    : return `${todayYear}-${todayMonth}-${dayAgo}`;
    case 'week'   : return `${todayYear}-${todayMonth}-${weekAgo}`;
    case 'month'  : return `${todayYear}-${monthAgo}-${todayDate}`;
    case 'ytd'    : return `${todayYear}-01-01`;
    case 'year'   : return `${todayYear - 1}-${todayMonth}-${todayDate}`;
    default       : return "2013-01-01";
    
  }
  
}